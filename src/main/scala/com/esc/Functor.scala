package com.esc

import scala.language.higherKinds

/**
  * This is the definition of a Functor, something that given an f:A=>B, gives us a way to turn F[A] into F[B]
  *
  * @tparam F
  */
trait Functor[F[_]] {
  def map[A, B](fa: F[A])(f: A => B): F[B]
}

/**
  * Functor implementation for Lists of anything
  *
  */
object Functor {

  implicit  def listFunctor: Functor[List] = new Functor[List] {
    override def map[A, B](fa: List[A])(f: A => B): List[B] = fa.map(f)
  }

}
