package com.esc

import scala.concurrent.Future
import scala.language.higherKinds

import scala.concurrent.ExecutionContext.Implicits.global

case class TicketLock(id: String)
case class Concert(name: String)
case class Ticket(id: String, concert:String)

/**
  * A business method
  * @tparam F
  */
trait TicketService[F[_]] {
  def reserveTicket(ticketId: String): F[TicketLock]
}

object TicketService {
  implicit def defaultTicketService: TicketService[Future] = new TicketService[Future] {
    override def reserveTicket(ticketId: String): Future[TicketLock] = Future {
      TicketLock(ticketId)
    }
  }
}

/**
  * Business method
  *
  * @tparam F
  */
trait ConcertService[F[_]] {
  def resolveConcert(ticketId: String): F[Concert]
}

object ConcertService {
  implicit def defaultConcertService: ConcertService[Future] = new ConcertService[Future] {
    override def resolveConcert(ticketId: String): Future[Concert] = Future {
      Concert("Default Concert")
    }
  }
}

object Tagless {
  def complexReserve[F[_]](id: String)(implicit TS: TicketService[F], CS: ConcertService[F], M: Monad[F]): F[Ticket] = {
    val ticketLock: F[TicketLock] = TS.reserveTicket(id)
    val concert: F[Concert] = CS.resolveConcert(id)

    M.flatMap(ticketLock)(lock => M.flatMap(concert)(concert => M.pure(Ticket(lock.id, concert.name))))

    //    for {
    //      lock <- TS.reserveTicket(id)
    //      concert <- CS.resolveConcert(lock.)
    //    }
  }
}

object Main extends App {
  import Tagless._
  import TicketService.defaultTicketService
  import ConcertService.defaultConcertService

  implicit class MonadOps[F[_], A](fa: F[A])(implicit M: Monad[F]) {

    def flatMap[B](f: A => F[B]): F[B] = M.flatMap(fa)(f)

    def map[B](f: A => B): F[B] = M.map(fa)(f)
  }

  private val eventualTicket: Future[Ticket] = complexReserve("ticketId")
  eventualTicket.foreach(ticket => println(s"ticket=$ticket"))
}
