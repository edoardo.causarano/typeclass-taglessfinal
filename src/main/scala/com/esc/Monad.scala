package com.esc

import scala.language.higherKinds

/**
  * *It*
  *
  * @tparam F
  */
trait Monad[F[_]] extends Functor[F] {
  def pure[A](a: A): F[A]
  def flatMap[A, B](fa: F[A])(f: A => F[B]): F[B]
}

/**
  * Implementation for Lists
  */
object Monad {
  implicit val listMonad: Monad[List] = new Monad[List] {
    override def pure[A](a: A): List[A] = List(a)
    override def flatMap[A, B](fa: List[A])(f: A => List[B]): List[B] = fa.flatMap(f)

    override def map[A, B](fa: List[A])(f: A => B): List[B] = fa.flatMap(a => pure(f(a)))

  }
}
