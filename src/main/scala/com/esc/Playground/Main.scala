package com.esc.Playground

import com.esc.{Functor, Monad, Monoid}

import scala.language.higherKinds

object MainMonoid extends App {
  def fold[A](list: List[A])(implicit m: Monoid[A]): A = {
    list.foldLeft(m.zero)(m.+)
  }

  println(fold(List(1,2,3,4)))

}

object MainFunctor extends App {

  def foo(a: Int): String = a.toString

  val xs: List[String] = List(1,2,3).map(foo)

  def nTimes[F[_], A](fa: F[A], n: Int)(f: A => A)(implicit G: Functor[F]): F[A] = {
    def iter(acc: F[A], i: Int): F[A] = if (i == 0) acc else {
      iter(G.map(acc)(f), i - 1)
    }
    iter(fa, n)
  }

  println(nTimes(List(1, 2, 3), 2)(x => x + 1))
}

object MonadMain extends App {

  import Monad.listMonad._
  def foo[F[_]](a: Int)(implicit F: Monad[F]): F[Int] =
    F.flatMap(F.pure(5))(a => F.pure(a + 1))

  println(List(1, 2, 3).map(a => List(1 + a)))
  println(map(List(1, 2, 3))(a => List(1 + a)))

  println(List(1, 2, 3).flatMap(a => List(1 + a)))
  println(flatMap(List(1, 2, 3))(a => List(1 + a)))

}
