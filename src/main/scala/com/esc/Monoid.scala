package com.esc

/**
  * Monoids are the combination of a set, an internal operation called sum and a neutral element wrt this sum
  *
  * @tparam A
  */
trait Monoid[A] {
  def +(a: A, b: A): A
  def zero: A
}

object Monoid {

  /**
    * Implementation for strings
    *
    * @return
    */
  implicit def stringMonoid: Monoid[String] = new Monoid[String] {
    override def +(a: String, b: String): String = a.concat(b)

    override def zero: String = ""
  }

  /**
    * Implementation for integers
    *
    * @return
    */
  implicit def IntegerMonoid: Monoid[Int] = new Monoid[Int] {
    override def +(a: Int, b: Int): Int = a + b

    override def zero: Int = 0
  }
}
